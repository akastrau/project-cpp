package pl.edu.pg.ftims.schoolSimulator;


import java.util.concurrent.Semaphore;

public class CalculateAverageThread implements Runnable {
    Register register;
    private int step;
    private int index;
    private final Semaphore groupsemaphore = new Semaphore(1);

    public CalculateAverageThread(Register register, int step, int index) {
        this.register = register;
        this.step = step;
        this.index = index;
    }

    public void calculateAverage(){
        int startID = index*step;
        int endID = startID + step;
        if (endID>register.getNextIndex()){
            endID=register.getNextIndex();
        }
        for (int i=startID;i<endID;i++){
            Student s1 =  register.getRegister().get(i);
            s1.lock();
            s1.calculateAverage();
            s1.unlock();

        }
    }

    public void calculateGroupAverage(){
        int startID = index*step;
        int endID = startID + step;
        if (endID>register.getNextIndex()){
            endID=register.getNextIndex();
        }
        for (int i=startID;i<endID;i++){
            try {
                groupsemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Student s1 =  register.getRegister().get(i);

            //s1.lock();
           // double avr =s1.getAverage();
           // s1.unlock();
            register.setGroupAverage(s1.getAverage()/(double)register.getNextIndex());
            groupsemaphore.release();

        }
    }

    @Override
    public void run() {
        calculateAverage();
        calculateGroupAverage();
    }
}
