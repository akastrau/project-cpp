package pl.edu.pg.ftims.schoolSimulator;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class CalculateThreadMenagment {
    private int amountOfThreads;
    private Register register;
    private ExecutorService executorService;

    public CalculateThreadMenagment(int amountOfThreads, Register register) {
        this.amountOfThreads = amountOfThreads;
        this.register = register;
        double step = Math.ceil((double) register.getNextIndex()/ (double) amountOfThreads);
        int step2 = (int) step;
        executorService = Executors.newScheduledThreadPool(amountOfThreads);
        for (int i=0;i<amountOfThreads;i++){
            executorService.submit(new CalculateAverageThread(register,step2,i));
        }
        executorService.shutdown();
        //Wprowadzilem timeout
        try {
            executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
