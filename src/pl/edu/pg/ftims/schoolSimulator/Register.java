package pl.edu.pg.ftims.schoolSimulator;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class Register {
    ConcurrentHashMap<Integer,Student> register;
    AtomicInteger nextIndex;
    private double groupAverage;
    private Semaphore semaphore = new Semaphore(1);

    public Register() {
        nextIndex = new AtomicInteger(0);
        groupAverage = 0;
        register = new ConcurrentHashMap<>();
    }

    public void addStudentToRegister(Student student){
        register.put(nextIndex.get(),student);
        nextIndex.incrementAndGet();
    }


    public void setGroupAverage(double groupAverage) {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.groupAverage += groupAverage;
        semaphore.release();
    }

    public double getGroupAverage() { return groupAverage; }

    public int getNextIndex() {
        return nextIndex.get();
    }

    public ConcurrentHashMap<Integer, Student> getRegister() {
        return register;
    }

    @Override
    public String toString() {
        String result = "pl.edu.pg.ftims.schoolSimulator.Register \n";
        for (int i=0; i < nextIndex.get(); i++){
            result += register.get(i).toString() + "\n";
        }
        result+="\t\t\tGroup average: "+ String.format("%.2f", groupAverage) +"\n";
        return  result;
    }
}
