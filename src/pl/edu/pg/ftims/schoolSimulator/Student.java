package pl.edu.pg.ftims.schoolSimulator;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Student extends ReentrantLock {
    private String name;
    private String surname;
    private List<Double> grades;
    private double average;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Double> getGrades() {
        return grades;
    }

    public void setGrades(List<Double> grades) {
        this.grades = grades;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.average = 0;
        this.grades = new LinkedList<Double>();

    }

    public Student(boolean fair, String name, String surname, List<Double> grades, double average) {
        this.name = name;
        this.surname = surname;
        this.grades = grades;
        this.average = average;
    }

    public void addGrade(double grade){
        grades.add(grade);
    }

    @Override
    public String toString() {
        return (name+ " " + surname +" grades: {" + grades+" } "+ average );
    }

    public void calculateAverage(){
        double sum =0;
        for (Double el: grades) {
            sum += el;
        }
        this.average= sum / grades.size();
    }
}
