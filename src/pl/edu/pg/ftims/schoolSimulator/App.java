package pl.edu.pg.ftims.schoolSimulator;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;


public class App {
    public static void main(String[] args) {
        int howManyStudents = 5;
        boolean exit = false;
        int howManyThreads = 10;
        Register r1 = new Register();

        for (int i = 0; i < howManyStudents; i++) {
            r1.addStudentToRegister(new Student("Student", String.valueOf(i)));
        }
        ExecutorService examineService = Executors.newFixedThreadPool(howManyThreads);

        Scanner scan1 = new Scanner(System.in);

        while(!exit) {
            List<Future> futures = new ArrayList<>();
            for (int i = 0; i < howManyThreads; i++){
                futures.add(examineService.submit((Callable<Void>) () -> {
                    new Examiner(r1, 5).run();
                    return null;
                }));
            }
            for (Future future : futures){
                try {
                    future.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            } //Wait for marks
            r1.setGroupAverage(-(r1.getGroupAverage())); //clean average
            CalculateThreadMenagment m1 = new CalculateThreadMenagment(100, r1);
            System.out.println(r1);


            System.out.println("Do you want to make another cycle of the program? Y/N");
            String scan = scan1.nextLine();

            if (scan.equalsIgnoreCase("N")) {
                exit = true;
            }
        }
        examineService.shutdown();
    }
}


