package pl.edu.pg.ftims.schoolSimulator;

import java.util.Random;

public class Examiner implements Runnable {
    private Register register;
    private int howManyGrades;

    public Examiner(Register register, int howManyGrades) {
        this.register = register;
        this.howManyGrades = howManyGrades;
    }

    private void examine(){
        Random randomGerenerator = new Random();
        for (int i=0;i<register.getNextIndex();i++)
            for (int j=0; j<howManyGrades; j++){
                Student student = register.getRegister().get(i);
                student.lock();
                student.addGrade(randomGerenerator.nextInt(5)+1);
                student.unlock();
            }
    }
    @Override
    public void run() {
        examine();
    }

}
