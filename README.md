# Concurrent and Parallel Programming Project #

# Our team #

* Jakub Grenz 155797
* Adrian Kastrau 155807
* Szymon Kaminski 155803

# Description #

This app is simulation of examiners and students. At the beginning we're adding students to register, after that we're calculating average mark for them.

# How to run #

Run:

    java -jar Project-CPP.jar